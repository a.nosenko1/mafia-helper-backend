FROM maven:3.8.6-openjdk-18-slim

WORKDIR /usr/src/app

ADD mvnw pom.xml ./
RUN ["/usr/local/bin/mvn-entrypoint.sh", "mvn", "verify", "clean", "--fail-never"]

COPY .mvn/ .mvn
COPY src ./src
RUN mvn clean package spring-boot:repackage -DskipTests

EXPOSE 8181
CMD ["java", "-jar", "/usr/src/app/target/mafia-helper-1.jar"]
