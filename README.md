## Гайд по запуску 

 Проверьте версию java, нужна 18.0.2.1, скачать тут https://jdk.java.net/18/. Распакуйте куда угодно, например, **C:\Program Files\Java\jdk-18.0.2.1**. Далее  нужно установить переменную среды **JAVA_HOME = C:\Program Files\Java\jdk-18.0.2.1** и в **PATH** в начало добавить **%JAVA_HOME%\bin**.
 
 Для проверки версии:
 ```sh
 java -version
 javac -version
 mvn -version 
 ```
 Теперь для запуска:
 - Клоним репу: `git clone https://gitlab.com/a.nosenko1/mafia-helper-backend.git `
 - Собираем: `docker build -t jarapp:release <путь до папаки с Dockerfile (папки проекта), например, T:\java\mafia-helper\>`
 - Запускаем: `docker run --name <name you want> -d -p 8181:<port you want> jarapp:release `
 
 Приложение доступно по http://127.0.0.1:<port you want>
 
 - список запущенных контейнеров `docker ps`
 - остановка `docker stop <name you want>`
 - удаление `docker rm <name you want>`
