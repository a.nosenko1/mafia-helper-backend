package ru.nsu.mafiahelperteam.mafiahelper.controllers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.nsu.mafiahelperteam.mafiahelper.config.OpenApiConfig;


@Configuration
@Import({OpenApiConfig.class})
@EnableAutoConfiguration(exclude = {SecurityAutoConfiguration.class, DataSourceAutoConfiguration.class})
@ComponentScan({
		"ru.nsu.mafiahelperteam.mafiahelper.controllers",
		"ru.nsu.mafiahelperteam.mafiahelper.config",
		"ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.authentication.services",
		"ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.entities",
		"ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.security.settings",
		"ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.user.store.services",
		"ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.users.controllers",
		"ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.date.service",
		"ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.token.api"
})
//@SpringBootApplication(exclude = { SecurityAutoConfiguration.class, DataSourceAutoConfiguration.class})
public class MafiaHelperApplication {

	public static void main(String[] args) {

		SpringApplication.run(MafiaHelperApplication.class, args);
	}

}
