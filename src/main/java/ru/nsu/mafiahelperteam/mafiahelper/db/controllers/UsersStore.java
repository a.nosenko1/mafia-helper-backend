package ru.nsu.mafiahelperteam.mafiahelper.db.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.entities.User;

import java.util.Optional;


@Service
public class UsersStore {
    private static final Logger logger = LoggerFactory.getLogger(UsersStore.class);

    public static Boolean save(User user) {
        logger.info("saved: Id: " + user.getId() + " Username: " + user.getUsername() + " pass: " + user.getPassword());
        return true;
    }

    public static Optional<User> find(String id) {
        logger.info("find(id): Id: " + id);

        // Запрос к БД на поиск юзера по id

        // Если не найден
        // logger.info("user not found(id): Id: " + id);
        // return Optional.empty();

        // Если найден:
        String uid = "";
        String username = "";
        String password = "";

        final User user = User
                .builder()
                .id(uid)
                .username(username)
                .password(password)
                .build();

        logger.info("user found(id): Id: " + id);
        return Optional.ofNullable(user);
    }

    public static Optional<User> findByUsername(String username) {
        logger.info("find(Username): Username: " + username);

        // Запрос к БД на поиск юзера по username

        // Если не найден
        // logger.info("user not found(Username): username: " + username);
        // return Optional.empty();

        // Если найден:
        String uid = "";
        String password = "";

        final User user = User
                .builder()
                .id(uid)
                .username(username)
                .password(password)
                .build();

        logger.info("user found(Username): Username: " + username);
        return Optional.ofNullable(user);
    }

    public static Optional<User> findByToken(String token) {
        logger.info("find(token): token: " + token);

        // Запрос к БД на поиск юзера по token

        // Если не найден
        // logger.info("user not found(token): token: " + token);
        // return Optional.empty();

        // Если найден:
        String uid = "";
        String username = "";
        String password = "";

        final User user = User
                .builder()
                .id(uid)
                .username(username)
                .password(password)
                .build();

        logger.info("user found(id): Id: " + uid + " token: " + token);
        return Optional.ofNullable(user);
    }

    public static void setToken(String username, String token){
        logger.info("setToken: token: " + token + " username: " + username);
        // Запрос к БД на добавление токена для этого юзера
        // если вдруг у него уже установлен токен, заменить на новый
    }

    public static void deleteToken(String username){
        logger.info("deleteToken: username: " + username);
        // Запрос к БД на удаление токена для этого юзера
        // если вдруг у него нет токена, ничего не делать
    }


}
