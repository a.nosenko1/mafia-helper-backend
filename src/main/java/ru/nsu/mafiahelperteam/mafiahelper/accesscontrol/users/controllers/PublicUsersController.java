package ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.users.controllers;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.authentication.services.UserAuthenticationService;
import ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.entities.User;
import ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.security.settings.AuthExeption;
import ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.user.store.services.UserCrudService;

import java.util.Optional;
import java.util.UUID;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

/*
    Регистрация и логин
*/

@RestController
@RequestMapping("/public/users")
@CrossOrigin(origins={"chrome-extension://eelcnbccaccipfolokglfhhmapdchbfg"}, allowedHeaders = "*")
@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor(access = PACKAGE)
@Tag(name = "Public api", description = "Регистрация и логин")
final class PublicUsersController {
    @NonNull
    @Autowired
    UserAuthenticationService authentication;
    @NonNull
    UserCrudService users;

    @PostMapping("/register")
    @Operation(description = "Регистрация. Если пользователь с таким username существует, то будет попытка логина. "+
            "Если не существует - то будет создан и залогинен")
    String register(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password) throws AuthExeption {

        users
                .save(
                        User
                                .builder()
                                .id(username)
                                .username(username)
                                .password(password)
                                .build()
                );
        // если пользователь существует, то он не добавится
        // если пароль верный, то залогинет
        return login(username, password);
    }

    @PostMapping("/login")
    @Operation(description = "Логин. Если пароль неверный, то вернется 401. Если верный, вернется токен")
    String login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password) throws AuthExeption {

        return authentication.login(username, password).orElseThrow(() -> new AuthExeption("invalid login and/or password"));
    }
}
