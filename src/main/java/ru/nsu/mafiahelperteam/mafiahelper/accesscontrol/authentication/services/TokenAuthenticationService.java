package ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.authentication.services;
import java.util.concurrent.ThreadLocalRandom;

import com.google.common.collect.ImmutableMap;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.entities.User;
import ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.token.api.TokenService;
import ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.user.store.services.UserCrudService;
import ru.nsu.mafiahelperteam.mafiahelper.db.controllers.UsersStore;

import java.util.Objects;
import java.util.Optional;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

@Primary
@Service
@AllArgsConstructor(access = PACKAGE)
@FieldDefaults(level = PRIVATE, makeFinal = true)
final class TokenAuthenticationService implements UserAuthenticationService {
    @NonNull
    TokenService tokens;
    @NonNull
    UserCrudService users;

    private static final Logger logger = LoggerFactory.getLogger(UsersStore.class);

    @Override
    public Optional<String> login(final String username, final String password) {
        Optional<User> user_mb = users
                                    .findByUsername(username)
                                    .filter(user -> Objects.equals(password, user.getPassword()));

        if (user_mb.isEmpty()) {
            logger.info("Запрос на авторизацию отклонён (login: " + username + " | password: " + password + ")");
            return Optional.empty();
        }

        users.deleteToken(username);

        String token = String.valueOf(ThreadLocalRandom.current().nextInt(100, 999 + 1));

        users.setToken(username, token);
        logger.info("Запрос на авторизацию подтвержден (login: " + username + " | token: " + token + ")");
        return Optional.of(token);

//        return users
//              .findByUsername(username)
//              .filter(user -> Objects.equals(password, user.getPassword()))
//              .map(user -> tokens.expiring(ImmutableMap.of("username", username)));
    }

    @Override
    public Optional<User> findByToken(final String token) {
        return users.findByToken(token);

//        return Optional
//                .of(tokens.verify(token))
//                .map(map -> map.get("username"))
//                .flatMap(users::findByUsername);
    }

    @Override
    public void logout(final User user) {
        logger.info("Запрос на logout (login: " + user.getUsername());
        users.deleteToken(user.getUsername());
    }

}