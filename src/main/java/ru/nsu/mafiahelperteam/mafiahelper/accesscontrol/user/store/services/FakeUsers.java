package ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.user.store.services;

import org.springframework.stereotype.Service;
import ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.entities.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.nsu.mafiahelperteam.mafiahelper.db.controllers.UsersStore;

import java.util.*;

import static java.util.Optional.ofNullable;

@Service
public class FakeUsers implements UserCrudService {

    private static final Logger logger = LoggerFactory.getLogger(UsersStore.class);

    Map<String, User> users = new HashMap<>();
    Map<String, String> usersTokens = new HashMap<>(); // token, username

    @Override
    public Boolean save(User user) {
        // Если юзер есть, то ничего не делать
        // Если нет, то запрос в БД на сохранение юзера
        if (findByUsername(user.getUsername()).isPresent()){
            logger.info("user exist: Id: " + user.getId());
            return false;
        }

        users.put(user.getId(), user);
        UsersStore.save(user);

        logger.info("saved user: Id: " + user.getId());
        return true;
    }

    @Override
    public Optional<User> find(String id) {
        // ищет по id
        UsersStore.find(id);

        return ofNullable(users.get(id));
    }

    @Override
    public Optional<User> findByUsername(String username) {
        // ищет по username
        if (username == null){
            return Optional.empty();
        }
        UsersStore.findByUsername(username);
        logger.info("findByUsername: username: " + username);

        return users
                .values()
                .stream()
                .filter(u -> Objects.equals(username, u.getUsername()))
                .findFirst();
    }

    @Override
    public Optional<User> findByToken(String token) {
        logger.info("findByToken: token: " + token);
        // ищет по token
        UsersStore.findByToken(token);

        String username = usersTokens.get(token);
        logger.info("findByToken: token: " + token + username);
        return findByUsername(username);
    }

    @Override
    public void setToken(String username, String token) {
        UsersStore.setToken(username, token);

        logger.info("setToken: username: " + username + " token: " + token);
        usersTokens.put(token, username);
    }

    @Override
    public void deleteToken(String username) {
        UsersStore.deleteToken(username);

        logger.info("deleteToken: username: " + username + usersTokens.toString());

        Optional<String> token = Optional.empty();

        Iterator it = usersTokens.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if (pair.getValue() == username) {
                System.out.println("Found: " + pair.getKey() + " = " + pair.getValue());
                token = Optional.of(pair.getKey().toString());
            }
            it.remove();
        }

        if (token.isEmpty()) return;
        usersTokens.remove(token.get());
        logger.info(usersTokens.toString() + " " + token.get());
    }
}
