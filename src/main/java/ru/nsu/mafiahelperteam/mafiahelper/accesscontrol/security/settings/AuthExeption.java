package ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.security.settings;

public class AuthExeption extends Exception {
    public AuthExeption() { super(); }
    public AuthExeption(String message) { super(message); }
    public AuthExeption(String message, Throwable cause) { super(message, cause); }
    public AuthExeption(Throwable cause) { super(cause); }
}
