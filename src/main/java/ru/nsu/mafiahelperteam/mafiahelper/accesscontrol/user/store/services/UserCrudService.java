package ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.user.store.services;

import ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.entities.User;

import java.util.Optional;

public interface UserCrudService {
    Boolean save(User user);

    Optional<User> find(String id);

    Optional<User> findByUsername(String username);

    Optional<User> findByToken(String token);

    void setToken(String username, String token);

    void deleteToken(String username);
}
