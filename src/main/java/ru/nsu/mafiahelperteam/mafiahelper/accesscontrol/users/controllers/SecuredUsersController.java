package ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.users.controllers;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.authentication.services.UserAuthenticationService;
import ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.entities.User;

import static lombok.AccessLevel.PACKAGE;
import static lombok.AccessLevel.PRIVATE;

@RestController
@RequestMapping("/users")
@FieldDefaults(level = PRIVATE, makeFinal = true)
@AllArgsConstructor(access = PACKAGE)
@Tag(name = "Secured api")
final class SecuredUsersController {
    @NonNull
    @Autowired
    UserAuthenticationService authentication;

    @GetMapping("/current")
    @Operation(description ="Просмотр текущего пользователя")
    User getCurrent(@AuthenticationPrincipal final User user) {
        return user;
    }

    @GetMapping("/logout")
    @Operation(description ="Разлогинивание")
    boolean logout(@AuthenticationPrincipal final User user) {
        authentication.logout(user);
        return true;
    }
}