package ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.date.service;

import org.joda.time.DateTime;
public interface DateService {
    DateTime now();
}
