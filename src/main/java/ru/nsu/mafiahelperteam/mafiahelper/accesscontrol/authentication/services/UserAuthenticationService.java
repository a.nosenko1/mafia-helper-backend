package ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.authentication.services;

import ru.nsu.mafiahelperteam.mafiahelper.accesscontrol.entities.User;

import java.util.Optional;

public interface UserAuthenticationService {
    Optional<String> login(String username, String password);

    Optional<User> findByToken(String token);

    void logout(User user);

}
